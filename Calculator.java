class Calculator{
	public static double add (double firstNum, double secNum){
		double resultAdd = firstNum + secNum;
		return resultAdd;
	}
	
	public static double substract (double firstNum, double secNum){
		double resultSubstract = firstNum - secNum;
		return resultSubstract;
	}
	
	public double multiply (double firstNum, double secNum){
		double resultMultiply = firstNum*secNum;
		return resultMultiply;
	}
	
	public double divide (double firstNum, double secNum){
		double resultDivide = firstNum/secNum;
		return resultDivide;
	}
}