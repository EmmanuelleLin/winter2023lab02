import java.util.Scanner;
class Part3{
	public static void main(String[]args){
		
		Scanner sc = new Scanner(System.in);
		Calculator calc = new Calculator();
		double firstInput = sc.nextDouble();
		double secInput = sc.nextDouble();
		
		double added = Calculator.add(firstInput,secInput);
		double substracted = Calculator.substract(firstInput,secInput);
		
		double multiplied = calc.multiply(firstInput,secInput);
		double divided = calc.divide(firstInput,secInput);
		
		System.out.println("add " + added);
		System.out.println("substract " + substracted);
		System.out.println("multiply " + multiplied);
		System.out.println("divide " + divided);
	}
}