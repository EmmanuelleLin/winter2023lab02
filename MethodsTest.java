class MethodsTest{
	public static void main (String[] args){
		/*int x = 5;
		double y = 1.2;
		methodTwoInputNoReturn(x,y);
		int z = methodNoInputReturnInt();
		System.out.println(z);
		int x = 9;
		int y = 5;
		System.out.println(sumSquareRoot(x,y));
		String s1 = "java";
		String s2 = "programming";
		System.out.println(s1.length());*/
		SecondClass sc = new SecondClass();
		int meow = sc.addTwo(50);
		System.out.println(meow);
		
	}
	public static void methodNoInputNoReturn(){
		System.out.println("I'm in a method that takes no input and returns nothing");
		int x = 20;
		System.out.println(x);
	}
	
	public static void methodOneInputNoReturn(int secondnum){
		System.out.println("Inside the method one input no return");
		secondnum -= 5;
		System.out.println(secondnum);
	}
	
	public static void methodTwoInputNoReturn(int thirdnum, double fourthnum){
		System.out.println(thirdnum);
		System.out.println(fourthnum);
	}
	
	public static int methodNoInputReturnInt(){
		return 5;
	}
	
	public static double sumSquareRoot(int fifthnum, int sixthnum){
		double added = Math.sqrt(fifthnum + sixthnum);
		return added;
	}
}